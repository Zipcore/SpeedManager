native float SpeedManager_GetCurrent(int iClient, float &fCurrent); // Current

native float SpeedManager_GetBasic(int iClient, float &fBasic); // Default
native float SpeedManager_GetBonus(int iClient, float &fBonus); // Buff

native float SpeedManager_GetLimit(int iClient, float &fLimit); // Slow
native float SpeedManager_GetMax(int iClient, float &fMax); // Max

native float SpeedManager_Update(int iClient); // Update

/**
 * Set basic speed rules.
 *
 * @param iClient		The client index.
 * @param fBasic		Basic speed.
 * @param fBasicMax	Max speed for speed buffs.
 * @param bReset		Remove all buffs.
 * @noreturn 			 
 */
native void SpeedManager_SetBasic(int iClient, float fBasic, float fBasicMax, bool bReset);

/**
 * Override the client max speed.
 *
 * @param iClient		The client index.
 * @param fLimit		Limit max speed.
 * @param fTime		If set to 0 override max speed.
 * @noreturn 			 
 */
native void SpeedManager_SetLimit(int iClient, float fLimit, float fTime);

/**
 * Give bonus speed which is added to the default speed.
 *
 * @param iClient		The client index.
 * @param fBonus		Bonus speed.
 * @param fTime		If set to 0 override default speed.
 * @noreturn 			 
 */
native void SpeedManager_AddBonus(int iClient, float fBonus, float fTime);