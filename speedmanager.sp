#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Speed Manager"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_DESCRIPTION "Simple speed natives and forwards to share speed rules"
#define PLUGIN_URL "zipcore.net"

#include <sourcemod>
#include <sdktools>
#include <smlib>
#include <speedmanager>

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && IsPlayerAlive(%1))

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	RegPluginLibrary("speedmanager");
	
	CreateNative("SpeedManager_GetCurrent", Native_GetCurrent);
	CreateNative("SpeedManager_GetBasic", Native_GetBasic);
	CreateNative("SpeedManager_GetBonus", Native_GetBonus);
	CreateNative("SpeedManager_GetLimit", Native_GetLimit);
	CreateNative("SpeedManager_GetMax", Native_GetMax);
	CreateNative("SpeedManager_SetBasic", Native_SetBasic);
	CreateNative("SpeedManager_SetLimit", Native_SetLimit);
	CreateNative("SpeedManager_AddBonus", Native_AddBonus);
	CreateNative("SpeedManager_Update", Native_Update);
	
	return APLRes_Success;
}

public void OnPluginStart()
{
	CreateConVar("speedmanager_version", PLUGIN_VERSION, "Speed Manager version", FCVAR_DONTRECORD|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
}

public void OnMapStart()
{
	CreateTimer(0.1, Timer_Check, _, TIMER_REPEAT | TIMER_FLAG_NO_MAPCHANGE);
}

float g_fSpeedCurrent[MAXPLAYERS + 1];

float g_fSpeedBasic[MAXPLAYERS + 1];
float g_fSpeedMax[MAXPLAYERS + 1];

float g_fSpeedBonus[MAXPLAYERS + 1];
float g_fSpeedBonusTime[MAXPLAYERS + 1];

float g_fSpeedLimit[MAXPLAYERS + 1];
float g_fSpeedLimitTime[MAXPLAYERS + 1];

void ResetSpeed(int iClient)
{
	g_fSpeedBasic[iClient] = 1.0;
	g_fSpeedMax[iClient] = 1.0;
	
	g_fSpeedBonus[iClient] = 0.0;
	g_fSpeedBonusTime[iClient] = 0.0;
	
	g_fSpeedLimit[iClient] = 1.0;
	g_fSpeedLimitTime[iClient] = 0.0;
}

void UpdateSpeed(int iClient)
{
	float fSpeed = g_fSpeedBasic[iClient];
	
	float fTime = GetGameTime();
	
	// Bonus speed
	if(g_fSpeedBonus[iClient] > 0.0 && g_fSpeedBonusTime[iClient] > fTime)
		fSpeed += g_fSpeedBonus[iClient];
	
	// Slow buff active, always overrides bonus speed
	if(g_fSpeedLimit[iClient] < fSpeed && g_fSpeedLimitTime[iClient] > fTime)
		fSpeed = g_fSpeedLimit[iClient];
	
	// Max speed
	if(g_fSpeedMax[iClient] < fSpeed)
		fSpeed = g_fSpeedMax[iClient];
	
	// Update speed if changed
	if(g_fSpeedCurrent[iClient] != fSpeed)
	{
		SetEntPropFloat(iClient, Prop_Data, "m_flLaggedMovementValue", fSpeed);
		g_fSpeedCurrent[iClient] = fSpeed;
	}
}

public Action Timer_Check(Handle timer, any data)
{
	LoopAlivePlayers(iClient)
		UpdateSpeed(iClient);
	
	return Plugin_Continue;
}

public int Native_GetCurrent(Handle plugin, int numParams)
{
	SetNativeCellRef(2, g_fSpeedCurrent[GetNativeCell(1)]);
}

public int Native_GetBasic(Handle plugin, int numParams)
{
	SetNativeCellRef(2, g_fSpeedBasic[GetNativeCell(1)]);
}

public int Native_GetBonus(Handle plugin, int numParams)
{
	SetNativeCellRef(2, g_fSpeedBonus[GetNativeCell(1)]);
}

public int Native_GetLimit(Handle plugin, int numParams)
{
	SetNativeCellRef(2, g_fSpeedLimit[GetNativeCell(1)]);
}

public int Native_GetMax(Handle plugin, int numParams)
{
	SetNativeCellRef(2, g_fSpeedMax[GetNativeCell(1)]);
}

public int Native_SetBasic(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	
	// Reset buffs
	if(GetNativeCell(4))
		ResetSpeed(iClient);
	
	g_fSpeedBasic[iClient] = GetNativeCell(2);
	g_fSpeedMax[iClient] = GetNativeCell(3);
	
	UpdateSpeed(iClient);
}

public int Native_SetLimit(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	float fLimit = GetNativeCell(2);
	float fTime = GetNativeCell(3);
	
	if(fTime == 0.0)
	{
		g_fSpeedMax[iClient] = fLimit;
	}
	else
	{
		g_fSpeedLimit[iClient] = fLimit;
		g_fSpeedLimitTime[iClient] = GetGameTime() + fTime;
	}
	
	UpdateSpeed(GetNativeCell(1));
}

public int Native_AddBonus(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	float fBonus = GetNativeCell(2);
	float fTime = GetNativeCell(3);
	
	if(fTime == 0.0)
	{
		g_fSpeedBasic[iClient] += fBonus;
		if(g_fSpeedMax[iClient] < g_fSpeedBasic[iClient])
			g_fSpeedBasic[iClient] = g_fSpeedMax[iClient];
	}
	else
	{
		g_fSpeedBonus[iClient] = fBonus;
		g_fSpeedBonusTime[iClient] = GetGameTime() + fTime;
	}
	
	UpdateSpeed(GetNativeCell(1));
}

public int Native_Update(Handle plugin, int numParams)
{
	UpdateSpeed(GetNativeCell(1));
}